import cv2
import sys
import datetime
from pyzbar.pyzbar import decode, ZBarSymbol
import numpy as np


# via https://note.nkmk.me/en/python-pyzbar-barcode-qrcode/

def print_data(str_data):
    """Prints the string with a timestamp attached

    Args:
        str_data (str): a string
    """
    now = datetime.datetime.now()
    now_str = now.strftime("%d/%m/%Y, %H:%M:%S")
    print(f'{now_str} - "{str_data}"')


def preprocess_frame(frame):
    return cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)


def detect_qrcodes(id_webcam=0):
    """Opens a video feed, detects and decodes any QR codes shown to the camera

    Args:
        id_webcam (int, optional): the id number of the video device used to capture the feed. Defaults to 0.
    """
    webcam = cv2.VideoCapture(id_webcam)

    if webcam is None or not webcam.isOpened():
        print(
            f'Warning: unable to open video source: {id_webcam}', file=sys.stderr)
        sys.exit(1)

    webcam.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
    webcam.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
    webcam.set(cv2.CAP_PROP_AUTOFOCUS, 1)
    webcam.set(cv2.CAP_PROP_FOCUS, 30)
    webcam.set(cv2.CAP_PROP_FPS, 20)
    webcam.set(cv2.CAP_PROP_EXPOSURE, 128)

    webcam_is_active, frame = webcam.read()

    while webcam_is_active:
        # Converting to RGB yields better detection results
        processed_frame = preprocess_frame(frame)

        # For each value in a list of values decoded from the QR code(s)
        for d in decode(processed_frame, symbols=[ZBarSymbol.QRCODE]):
            decoded_string = d.data.decode()
            print_data(decoded_string)

            # Adds a rectangle around the QR code...
            frame = cv2.rectangle(frame, (d.rect.left, d.rect.top),
                                  (d.rect.left + d.rect.width, d.rect.top + d.rect.height), (255, 0, 0), 2)
            # ...and around the bounds
            frame = cv2.polylines(
                frame, [np.array(d.polygon)], True, (0, 255, 0), 2)

            # Displays the decoded string on the frame
            frame = cv2.putText(frame, decoded_string, (d.rect.left, d.rect.top + d.rect.height),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 255, 255), 1, cv2.LINE_AA)

        # Display the frame
        cv2.imshow("Webcam feed", frame)

        # Close the window by pressing "q"
        if cv2.waitKey(1) & 0xFF == ord('q'):
            webcam_is_active = False

        else:
            webcam_is_active, frame = webcam.read()

    webcam.release()
    cv2.destroyAllWindows()
    return


if __name__ == "__main__":
    detect_qrcodes()
