# py-opencv-qrcode

## Summary

Using your webcam and the _OpenCV_ Python library, this piece of code detects any QR codes you show in front of your webcam, decodes and outputs their data on the console

## Setup

You can install all the necessary requirements (the latest OpenCV library version) using `pip` and the `requirements.txt` file as follows:

```
git clone https://gitlab.com/alsagone/py-opencv-qrcode.git
cd py-opencv-qrcode/
pip install -r requirements.txt
python3 main.py
```
